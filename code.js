let btn = document.getElementById("btn");
let icono = document.getElementById("icono");
let pie = document.getElementById("pie");


let liquid1 = document.getElementById("liquid1");
let liquid2 = document.getElementById("liquid2");
let liquid3 = document.getElementById("liquid3");
let liquid4 = document.getElementById("liquid4");
let liquid5 = document.getElementById("liquid5");
let liquid6 = document.getElementById("liquid6");
let liquid7 = document.getElementById("liquid7");
let liquid8 = document.getElementById("liquid8");
let liquid9 = document.getElementById("liquid9");
let liquid10 = document.getElementById("liquid10");


let estadoBtn = false;

btn.addEventListener("click", () => {
    if (estadoBtn) {
        // Acciones cuando este abierto
        icono.classList.remove("abrir");
        pie.classList.remove("abrir-pie");

        // acciones cuando se cierre la tarjeta
        setTimeout(() => {
            liquid1.classList.remove("show");
            liquid2.classList.remove("show");
            liquid3.classList.remove("show");
            liquid4.classList.remove("show");
            liquid5.classList.remove("show");
            liquid6.classList.remove("show");
            liquid7.classList.remove("show");
            liquid8.classList.remove("show");
            liquid9.classList.remove("show");
            liquid10.classList.remove("show");
        }, 150);

        setTimeout(() => {
            liquid1.classList.remove("liquid-1-ocultar");
            liquid2.classList.remove("liquid-2-ocultar");
            liquid3.classList.remove("liquid-3-ocultar");
            liquid4.classList.remove("liquid-4-ocultar");
            liquid5.classList.remove("liquid-5-ocultar");
            liquid6.classList.remove("liquid-6-ocultar");
            liquid7.classList.remove("liquid-7-ocultar");
            liquid8.classList.remove("liquid-8-ocultar");
            liquid9.classList.remove("liquid-9-ocultar");
            liquid10.classList.remove("liquid-10-ocultar");
        }, 300);

        estadoBtn = false;
    } else {
        // acciones cuandoe ste cerrado
        icono.classList.add("abrir");
        pie.classList.add("abrir-pie");

        // acciones cuando se abra la tarjeta

        setTimeout(() => {
            liquid1.classList.add("show");
            liquid2.classList.add("show");
            liquid3.classList.add("show");
            liquid4.classList.add("show");
            liquid5.classList.add("show");
            liquid6.classList.add("show");
            liquid7.classList.add("show");
            liquid8.classList.add("show");
            liquid9.classList.add("show");
            liquid10.classList.add("show");
        }, 200);
        setTimeout(() => {
            liquid1.classList.add("liquid-1-ocultar");
            liquid2.classList.add("liquid-2-ocultar");
            liquid3.classList.add("liquid-3-ocultar");
            liquid4.classList.add("liquid-4-ocultar");
            liquid5.classList.add("liquid-5-ocultar");
            liquid6.classList.add("liquid-6-ocultar");
            liquid7.classList.add("liquid-7-ocultar");
            liquid8.classList.add("liquid-8-ocultar");
            liquid9.classList.add("liquid-9-ocultar");
            liquid10.classList.add("liquid-10-ocultar");
        }, 400);

        estadoBtn = true;
    }
})